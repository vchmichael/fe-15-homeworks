import React, {Component, Fragment} from 'react';
import Modal from './components/Modal/Modal'
import './App.scss'
import Button from "./components/Button/Button";


class App extends Component {
    state = {
        successModal: {
            title: 'Success!',
            body: "Operation has been proceed successfully!",
            closeButton: true,
            actions: [
                <Button
                    key={0}
                    text={'OK'}
                    backgroundColor={'#00000030'}
                />,
            ],
            type: 'modal-success',
        },
        deleteModal: {
            title: 'Do you want to delete this file?',
            body: `Once you delete this file, it won’t be possible to undo this action.
               Are you sure you want to delete it?`,
            closeButton: true,
            actions: [
                <Button
                    key={0}
                    text={'OK'}
                    backgroundColor={'#00000030'}
                />,
                <Button
                    key={1}
                    text={'Cancel'}
                    backgroundColor={'#00000030'}
                />
            ],
            type: 'modal-delete',
        },
        isActive: false,
        activeModal: null
    }

    render() {
        const whichModal = this.state[this.state.activeModal]
        return (
            <Fragment>
                <div className={'btns'}>
                    <Button
                        text={'Success'}
                        backgroundColor={'green'}
                        fn={this.openSuccessModal.bind(this)}
                    />
                    <Button
                        text={'Delete'}
                        backgroundColor={'red'}
                        fn={this.openDeleteModal.bind(this)}
                    />
                </div>
                    {this.state.isActive && <Modal
                    title={whichModal.title}
                    body={whichModal.body}
                    actions={whichModal.actions}
                    closeBtn={whichModal.closeButton}
                    closeFn={this.closeModal.bind(this)}
                    type={whichModal.type}

                    />}

            </Fragment>
        );
    }
    openSuccessModal() {
        this.setState({
            isActive: true,
            activeModal: 'successModal'
        })

    }
    openDeleteModal() {
        this.setState({
            isActive: true,
            activeModal: 'deleteModal'
        })

    }
    closeModal() {
        this.setState((curState) =>{
            return {
                ...curState,
                isActive: !curState.isActive
            }});

    }


}

export default App;
