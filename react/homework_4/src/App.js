import React, {Fragment, useEffect} from 'react';
import './App.scss'
import Header from "./components/Header/Header";
import MainRoutes from "./routes/MainRoutes";
import {getProducts} from "./redux/actions";
import {useDispatch} from "react-redux";



const App = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getProducts())
    }, [])

    return (
        <Fragment>
            <Header/>
            <MainRoutes title={'Our Bikes'}/>
        </Fragment>
    );

}

export default App;
