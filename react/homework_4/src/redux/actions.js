//TYPES
export const OPEN_MODAL = 'OPEN_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'
export const ADD_FAVORITES = 'ADD_FAVORITES'
export const ADD_TO_CART = 'ADD_TO_CART '
export const GET_PRODUCTS = 'GET_PRODUCTS '
export const REMOVE_FROM_FAVORITES = 'REMOVE_FROM_FAVORITES'
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'

//ACTIONS

export const openModal = () => (dispatch) => {
    dispatch({type: OPEN_MODAL});
}

export const closeModal = () => (dispatch) => {
    dispatch({type: CLOSE_MODAL});
}

export const getProducts = () => (dispatch) => {
    fetch('./goods.json')
        .then(res => res.json())
        .then(data =>
            dispatch({type: GET_PRODUCTS, payload: data})
        )
}

export const addToFavorites = (id) => (dispatch) => {
    dispatch({type: ADD_FAVORITES, payload: id});
    localStorage.setItem(`favorites${id}`, true)
}

export const addToCart = (id) => (dispatch) => {
    localStorage.setItem(`cart${id}`, true)
    dispatch({type: ADD_TO_CART, payload: id});
}

export const removeFromFavorites = (id) => (dispatch) => {
    dispatch({type: REMOVE_FROM_FAVORITES, payload: id});
    localStorage.removeItem(`favorites${id}`)
}

export const removeFromCart = (id) => (dispatch) => {
    localStorage.removeItem(`cart${id}`)
    dispatch({type: REMOVE_FROM_CART, payload: id});
}


