import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss'
import {connect, useDispatch} from 'react-redux'
import {addToCart, closeModal, openModal, removeFromCart} from "../../redux/actions";


const Button = (props) => {
    const {text, backgroundColor, modal, id} = props;
    const style = {backgroundColor: backgroundColor}
    const dispatch = useDispatch();
    return (
        <button
            onClick={() => {
                text ==='Remove' ? dispatch(removeFromCart(id)) : dispatch(addToCart(id))
                !modal.isActive ?  dispatch(openModal()) : dispatch(closeModal())
            }}
            className="btns__btn"
            style={style}>{text}
        </button>
    );
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    fn: PropTypes.func,
    text: PropTypes.string
};

const mapStoreToProps = ({modal}) => ({modal});

export default connect(mapStoreToProps)(Button);