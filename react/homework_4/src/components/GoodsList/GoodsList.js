import React from 'react';
import GoodItem from '../GoodItem/GoodItem'
import './GoodsList.scss'
import {connect} from 'react-redux'

const GoodsList = (props) => {
    const {products=[], cartProducts=[], favoritesProduct=[]} = props
    return (
        <>
            <div className="wrapper">
                <div className='our-products'>
                    {cartProducts && cartProducts.map(e => <GoodItem product={e} key={e.article}  />)}
                    {favoritesProduct && favoritesProduct.map(e => <GoodItem product={e} key={e.article}  />)}
                    {cartProducts.length < 1 && favoritesProduct.length < 1 && products.map(e => <GoodItem product={e} key={e.article}  />)}
                </div>
            </div>
        </>
    );
}

// mapStateToProps
const mapStoreToProps = (state) => ({
    products: state.products.data
})

export default connect(mapStoreToProps)(GoodsList);