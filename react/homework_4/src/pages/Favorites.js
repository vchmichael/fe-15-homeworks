import React from 'react';
import GoodsList from "../components/GoodsList/GoodsList";
import './Favorites.scss'
import {connect} from "react-redux";

const Favorites =(props) => {
    const {products} = props
    const favoritesProduct = products ? products.filter(e => e.inFavorites) : []
    return (
        <div>
            <h2 className='section-title'>Favorites</h2>
            {favoritesProduct.length > 0 && <GoodsList favoritesProduct={favoritesProduct}/>}
            {favoritesProduct.length < 1 && <span className='empty-alert'>No items in your favorites yet</span>}
        </div>
    );

}

// mapStateToProps
const mapStoreToProps = (state) => ({
    products: state.products.data
})

export default connect(mapStoreToProps)(Favorites);