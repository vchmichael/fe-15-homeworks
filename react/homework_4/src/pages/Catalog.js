import React from 'react';
import GoodsList from "../components/GoodsList/GoodsList";

const Catalog = (props) => {
    const {title, changeUpdate} = props
        return (
            <>
                <h2 className='section-title'>{title}</h2>
                <GoodsList title={title} changeUpdate={changeUpdate}/>
            </>
        );
}

export default Catalog;