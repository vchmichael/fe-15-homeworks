import React from 'react';
import GoodsList from "../components/GoodsList/GoodsList";
import './Cart.scss'
import {connect} from "react-redux";

const Cart = (props) => {

    const {products} = props
    const cartProducts = products ? products.filter(e => e.inCart) : []

    return (
        <div>
            <h2 className='section-title'>Cart</h2>
            {cartProducts.length < 1 && <span className='empty-alert'>No items in your cart yet</span>}
            {cartProducts.length > 0 && <GoodsList cartProducts={cartProducts}/>}
        </div>
    );

}

// mapStateToProps
const mapStoreToProps = (state) => ({
    products: state.products.data
})

export default connect(mapStoreToProps)(Cart);