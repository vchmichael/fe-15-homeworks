import {combineReducers} from "redux";
import {modalReducer} from "./modalReducer";
import {productReducer} from "./productReducer";




export const rootReducer = combineReducers({
    modal:modalReducer,
    products: productReducer
})