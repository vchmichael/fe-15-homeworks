import React from 'react';
import GoodsList from "../components/GoodsList/GoodsList";
import {connect} from "react-redux";


const Catalog = (props) => {
    const {title, products} = props
        return (
            <>
                <h2 className='section-title'>{title}</h2>
                <GoodsList title={title} products={products} />
            </>
        );
}

const mapStoreToProps = (state) => ({
    products: state.products.data
})

export default connect(mapStoreToProps)(Catalog);