import React from 'react';
import GoodsList from "../components/GoodsList/GoodsList";
import './Cart.scss'
import {connect} from "react-redux";
import FullOrderForm from "../components/OrderForm/OrderForm";
import Modal from "../components/Modal/Modal";
import Button from "../components/Button/Button";

const Cart = (props) => {

    const {products, modal} = props
    const cartProducts = products.filter(e => e.inCart)

    return (
        <div className="wrapper-cart">
            <div>
                <h2 className='section-title'>Cart</h2>
                {cartProducts.length < 1 && <span className='empty-alert'>No items in your cart yet</span>}
                {cartProducts.length > 0 && <GoodsList products={cartProducts}/>}
            </div>
            {cartProducts.length > 0 &&
            <div className='form-wrapper'>
                <h2>Order Form</h2>
                <FullOrderForm products={cartProducts}/>
            </div>}
            {modal.isActive && modal.type === 'modal-success' && <Modal
                title={'Success'}
                body={'Your order is confirmed! Please, wait for manager call.'}
                actions={[<Button
                    key={0}
                    text={'OK'}
                    backgroundColor={'#00000030'}
                />]}
                closeBtn={true}
                type={modal.type}
            />}
        </div>
    );

}

// mapStateToProps
const mapStoreToProps = (state) => ({
    products: state.products.data,
    modal:state.modal
})

export default connect(mapStoreToProps)(Cart);