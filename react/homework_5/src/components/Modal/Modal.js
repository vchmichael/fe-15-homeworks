import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import './Modal.scss';
import Button from "../Button/Button";
import {useDispatch} from "react-redux";
import {closeModal} from "../../redux/actions";


const Modal = (props) => {
    const {title, body, closeBtn, actions, type} = props;
    const dispatch = useDispatch()
    return (
        <Fragment>
            <div className={type}>
                <div className={type + '__header'}>
                    <h2 className={type + '__header__title'}>{title}</h2>
                    {closeBtn && <Button text={'×'} />}
                </div>
                <div className={type + '__body'}>
                    <p>{body}</p>
                </div>
                <div className={type + '__footer'}>
                    {actions}
                </div>
            </div>
            <div className="overlay" onClick={() => dispatch(closeModal())}>
            </div>
        </Fragment>
    );

}

Modal.propTypes = {
    title: PropTypes.string,
    closeBtn: PropTypes.bool,
    body: PropTypes.string,
    actions: PropTypes.array,
    closeFn: PropTypes.func,
    type: PropTypes.string
}
export default Modal;