import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss'
import {connect, useDispatch} from 'react-redux'
import {addToCart, closeModal, openModal, removeFromCart} from "../../redux/actions";


const Button = (props) => {
    const {text, backgroundColor, modal, id} = props;
    const style = {backgroundColor: backgroundColor}
    const dispatch = useDispatch();
    return (
        <button
            onClick={() => {
                switch (text) {
                    case 'Remove' :
                        dispatch(removeFromCart(id))
                        dispatch(openModal())
                        break
                    case 'Add to cart':
                        dispatch(addToCart(id))
                        dispatch(openModal())
                        break
                    case 'OK':
                       dispatch(closeModal())
                        break
                    case '×':
                        dispatch(closeModal())
                        break
                }
            }}
            className="btns__btn"
            style={style}>{text}
        </button>
    );
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    fn: PropTypes.func,
    text: PropTypes.string
};

const mapStoreToProps = ({modal}) => ({modal});

export default connect(mapStoreToProps)(Button);