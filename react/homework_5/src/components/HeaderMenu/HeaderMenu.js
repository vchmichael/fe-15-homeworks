import React from 'react';
import {NavLink} from "react-router-dom";

const HeaderMenu =() =>  {

        return (
            <ul className='nav-list'>
                <li className='nav-item'>
                    <NavLink  activeClassName='active' to='/catalog'>Catalog</NavLink>
                </li>
                <li className='nav-item'>
                    <NavLink to='/favorites'>Favorites</NavLink>
                </li>
                <li className='nav-item'>
                    <NavLink  activeClassName='active' to='/cart'>Cart</NavLink>
                </li>
            </ul>
        );
}

export default HeaderMenu;