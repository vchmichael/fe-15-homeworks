import React from 'react';
import Button from "../Button/Button";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faStar} from '@fortawesome/free-solid-svg-icons'
import {faStar as starIcon} from "@fortawesome/free-regular-svg-icons"
import './GoodItem.scss'
import Modal from "../Modal/Modal";
import {connect, useDispatch} from 'react-redux'
import {addToFavorites, removeFromFavorites} from "../../redux/actions";

const GoodItem = (props) => {
    const {product, modal} = props
    const dispatch = useDispatch()

    return (
        <div className="product-card">
            <div className='product-header'>
                <span className='article'>Article: {product.article}</span>
                <a target="_blank" href="#"><img
                    src={product.imageUrl} alt="product"/></a>
                <h2>{product.name}</h2>
                <span>Color: {product.color}</span>
            </div>
            <div className="product-bio">
                <h4>{product.price} {product.currency}</h4>
                <span>
                    {product.inFavorites && <FontAwesomeIcon onClick={() => dispatch(removeFromFavorites(product.article))}  icon={faStar} className='favorites'/>}
                    {!product.inFavorites && <FontAwesomeIcon onClick={() => dispatch(addToFavorites(product.article))} icon={starIcon} className='favorites'/>}
                </span>
                {product.inCart && <Button text={'Remove'} backgroundColor={'#c70039'} id={product.article}/>}
                {!product.inCart && <Button text={'Add to cart'} backgroundColor={'#C70039'} id={product.article}/>}
            </div>

            {modal.isActive && modal.type === 'modal-success' && <Modal
                title={'Success'}
                body={'Done'}
                actions={[<Button
                    key={0}
                    text={'OK'}
                    backgroundColor={'#00000030'}
                />]}
                closeBtn={true}
                type={modal.type}
            />}
        </div>
    );

}

const mapStoreToProps = ({modal}) => ({modal})


export default connect(mapStoreToProps)(GoodItem);