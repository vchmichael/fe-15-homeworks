import React, {Fragment, useEffect} from 'react';
import './App.scss'
import Header from "./components/Header/Header";
import MainRoutes from "./routes/MainRoutes";
import {getData} from "./redux/actions";
import {useDispatch, useSelector} from "react-redux";
import Loader from "./components/Loader/Loader";


const App = () => {
    const dispatch = useDispatch();
    const goods = useSelector(state => state.products.data)

    useEffect(() => {
        dispatch(getData())
    }, [])
    return (
        <>
            {!goods && <Loader/>}
            {goods && <Fragment>
                <Header/>
                <MainRoutes title={'Our Bikes'}/>
            </Fragment>}
        </>
    );

}

export default App;
