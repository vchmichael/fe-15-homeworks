import React, {useState} from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Favorites from "../pages/Favorites";
import Cart from "../pages/Cart";
import Catalog from "../pages/Catalog";

const MainRoutes =(props) => {
    const {products, title} = props
    const prodFavorites = products.filter( e => localStorage.getItem(`favorites${e.article}`))
    const prodCart = products.filter( e => localStorage.getItem(`cart${e.article}`))
    const [update, setUpdate] = useState(false)
    const changeUpdate =() => {
        setUpdate(!update)
    }

        return (
            <>
                <Switch>
                    <Route exact path='/' render={() => <Redirect to='/catalog' />} />
                    <Route exact path='/favorites' render={() => <Favorites products={prodFavorites} changeUpdate={changeUpdate}/>}/>
                    <Route exact path='/cart' render={() => <Cart products={prodCart} changeUpdate={changeUpdate}/>}/>
                    <Route exact path='/catalog' render={() => <Catalog products={products} title={title} changeUpdate={changeUpdate}/>}/>
                </Switch>
            </>
        );
}

export default MainRoutes;