import React, {Fragment, useEffect, useState} from 'react';
import './App.scss'
import Header from "./components/Header/Header";
import MainRoutes from "./routes/MainRoutes";


const App = () => {
    const [products, setProducts] = useState(null)

    useEffect(() => {
        fetch('./goods.json')
            .then(res => res.json())
            .then(data =>
                setProducts(data)
            )
    }, [])

    return (
        <Fragment>
            <Header/>
            {products && <MainRoutes products={products} title={'Our Bikes'}/>}
        </Fragment>
    );

}

export default App;
