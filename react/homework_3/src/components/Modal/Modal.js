import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import './Modal.scss';
import Button from "../Button/Button";


const Modal = (props) => {
    const {title, body, closeBtn, actions, closeFn, type} = props;
    return (
        <Fragment>
            <div className={type}>
                <div className={type + '__header'}>
                    <h2 className={type + '__header__title'}>{title}</h2>
                    {closeBtn && <Button text={'×'} fn={closeFn}/>}
                </div>
                <div className={type + '__body'}>
                    <p>{body}</p>
                </div>
                <div className={type + '__footer'}>
                    {actions}
                </div>
            </div>
            <div className="overlay" onClick={closeFn}>
            </div>
        </Fragment>
    );

}

Modal.propTypes = {
    title: PropTypes.string,
    closeBtn: PropTypes.bool,
    body: PropTypes.string,
    actions: PropTypes.array,
    closeFn: PropTypes.func,
    type: PropTypes.string
}
export default Modal;