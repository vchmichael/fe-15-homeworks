import React from 'react';
import GoodItem from '../GoodItem/GoodItem'
import './GoodsList.scss'

const GoodsList = (props) => {
    const {products, changeUpdate} = props
    return (
        <>
            <div className="wrapper">
                <div className='our-products'>
                    {products.map(e => <GoodItem product={e} key={e.article} changeUpdate={changeUpdate}  />)}
                </div>
            </div>
        </>
    );
}

export default GoodsList;