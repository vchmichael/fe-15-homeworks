import React, {useState} from 'react';
import Button from "../Button/Button";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faStar} from '@fortawesome/free-solid-svg-icons'
import {faStar as starIcon} from "@fortawesome/free-regular-svg-icons"
import './GoodItem.scss'
import Modal from "../Modal/Modal";

const GoodItem = (props) => {
    const [inFavorites, setFavorite] = useState(false)
    const [inCart, setCart] = useState(false)
    const [addToCartModal, setAddToCartModal] = useState({
        title: 'Success!',
        body: "This product has been added to cart !",
        closeButton: true,
        actions: [
            <Button
                key={0}
                text={'OK'}
                backgroundColor={'#00000030'}
                fn={() => closeModalAdd()}
            />,
        ],
        type: 'modal-success',
        isActive: false
    })
    const [removeFromCartModal, setRemoveFromCartModal] = useState({
        title: 'Success!',
        body: "This product has been removed from cart !",
        closeButton: true,
        actions: [
            <Button
                key={0}
                text={'OK'}
                backgroundColor={'#00000030'}
                fn={() => closeModalRemove()}
            />,
        ],
        type: 'modal-success',
        isActive: false
    })
    const {product, changeUpdate} = props
    const addToFavorites = () => {
        setFavorite(!inFavorites)
        if (!localStorage.getItem(`favorites${product.article}`)) {
            localStorage.setItem(`favorites${product.article}`, true)
            changeUpdate()
        } else {
            localStorage.removeItem(`favorites${product.article}`)
            changeUpdate()
        }
    }
    const addToCart = () => {
        setCart(!inCart)
        if (!localStorage.getItem(`cart${product.article}`)) {
            localStorage.setItem(`cart${product.article}`, true)
            changeUpdate()

        } else {
            localStorage.removeItem(`cart${product.article}`)
            changeUpdate()
        }
    }
    const openAddCartModal = () => {
        setAddToCartModal(prevState => ({
                ...prevState,
                isActive: true
            }
        ))
    }
    const openRemoveCartModal = () => {
        setRemoveFromCartModal(prevState => ({
                ...prevState,
                isActive: true
            }
        ))
    }
    const closeModalAdd = () => {
        setAddToCartModal(prevState => ({
                ...prevState,
                isActive: false
            }
        ))
    }
    const closeModalRemove = () => {
        setRemoveFromCartModal(prevState => ({
                ...prevState,
                isActive: false
            }
        ))
    }
    return (
        <div className="product-card">
            <div className='product-header'>
                <span className='article'>Article: {product.article}</span>
                <a target="_blank" href="#"><img
                    src={product.imageUrl} alt="product"/></a>
                <h2>{product.name}</h2>
                <span>Color: {product.color}</span>
            </div>
            <div className="product-bio">
                <h4>{product.price} {product.currency}</h4>
                <span onClick={addToFavorites}>
                            {localStorage.getItem(`favorites${product.article}`) &&
                            <FontAwesomeIcon icon={faStar} className='favorites'/>}
                    {!localStorage.getItem(`favorites${product.article}`) &&
                    <FontAwesomeIcon icon={starIcon} className='favorites'/>}
                        </span>
                {localStorage.getItem(`cart${product.article}`) && <Button text={'Remove'} backgroundColor={'#C70039'} fn={openRemoveCartModal} removeCart={addToCart} />}
                {!localStorage.getItem(`cart${product.article}`) && <Button text={'Add to card'} backgroundColor={'#C70039'} fn={openAddCartModal} removeCart={addToCart}/>}

            </div>
            {addToCartModal.isActive && <Modal
                title={addToCartModal.title}
                body={addToCartModal.body}
                actions={addToCartModal.actions}
                closeBtn={addToCartModal.closeButton}
                closeFn={closeModalAdd}
                type={addToCartModal.type}
            />}
            {removeFromCartModal.isActive && <Modal
                title={removeFromCartModal.title}
                body={removeFromCartModal.body}
                actions={removeFromCartModal.actions}
                closeBtn={removeFromCartModal.closeButton}
                closeFn={closeModalRemove}
                type={removeFromCartModal.type}
            />}
        </div>
    );

}

export default GoodItem;