import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss'

const Button = (props) => {
    const {text, backgroundColor, fn, addCart, removeCart} = props;
    const style = {
        backgroundColor: backgroundColor
    }
    return (

        <button
            onClick={() => {
                fn();
                if (addCart) {
                    addCart();
                }
                if (removeCart) {
                    removeCart();
                }
            }}
            className="btns__btn"
            style={style}>{text}
        </button>

    );
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    fn: PropTypes.func,
    text: PropTypes.string
};

export default Button;