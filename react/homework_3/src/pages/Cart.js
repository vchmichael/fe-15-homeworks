import React from 'react';
import GoodsList from "../components/GoodsList/GoodsList";
import './Cart.scss'
const Cart =(props) => {

    const {products, changeUpdate} = props
    return (
        <div>
            <h2 className='section-title'>Cart</h2>
            <GoodsList products={products} changeUpdate={changeUpdate}/>
            {products.length < 1 && <span className='empty-alert'>No items in your cart yet</span>}
        </div>
    );

}

export default Cart;