import React from 'react';
import GoodsList from "../components/GoodsList/GoodsList";
import './Favorites.scss'

const Favorites =(props) => {
    const {products, changeUpdate} = props
    return (
        <div>
            <h2 className='section-title'>Favorites</h2>
            <GoodsList products={products} changeUpdate={changeUpdate} />
            {products.length < 1 && <span className='empty-alert'>No items in your favorites yet</span>}
        </div>
    );

}

export default Favorites;