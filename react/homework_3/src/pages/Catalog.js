import React from 'react';
import GoodsList from "../components/GoodsList/GoodsList";

const Catalog = (props) => {
    const {products, title, changeUpdate} = props
        return (
            <>
                <h2 className='section-title'>{title}</h2>
                <GoodsList products={products} title={title} changeUpdate={changeUpdate}/>
            </>
        );
}

export default Catalog;