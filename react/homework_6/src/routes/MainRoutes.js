import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import Favorites from "../pages/Favorites";
import Cart from "../pages/Cart";
import Catalog from "../pages/Catalog";

const MainRoutes = (props) => {
    const {title} = props
    return (
        <>
            <Switch>
                <Route exact path='/' render={() => <Redirect to='/catalog'/>}/>
                <Route exact path='/favorites' render={() => <Favorites />}/>
                <Route exact path='/cart' render={() => <Cart />}/>
                <Route exact path='/catalog' render={() => <Catalog title={title} />}/>
            </Switch>
        </>
    );
}

export default MainRoutes;