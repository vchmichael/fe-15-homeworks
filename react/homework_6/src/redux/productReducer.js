import {ADD_FAVORITES, ADD_TO_CART, GET_PRODUCTS, REMOVE_FROM_CART, REMOVE_FROM_FAVORITES, DELETE_FROM_LOCAL} from "./actions";

export function productReducer(state = [], action) {
    switch (action.type) {
        case GET_PRODUCTS:
            return {...state, data: action.payload}
        case ADD_TO_CART:
            localStorage.setItem(`cart${action.payload}`, 'true')
            return {
                ...state,
                data: state.data.map(e => {
                    return e.article === action.payload
                        ? {
                            ...e,
                            inCart: true,
                        }
                        : e
                })
            }
        case ADD_FAVORITES:
            localStorage.setItem(`favorites${action.payload}`, 'true')
            return {
                ...state,
                data: state.data.map(e => {
                    return e.article === action.payload
                        ? {
                            ...e,
                            inFavorites: true,
                        }
                        : e
                })
            }
        case REMOVE_FROM_FAVORITES:
            localStorage.removeItem(`favorites${action.payload}`)
            return {
                ...state,
                data: state.data.map(e => {
                    return e.article === action.payload
                        ? {
                            ...e,
                            inFavorites: false,
                        }
                        : e
                })
            }
        case REMOVE_FROM_CART:
            localStorage.removeItem(`cart${action.payload}`)
            return {
                ...state,
                data: state.data.map(e => {
                    return e.article === action.payload
                        ? {
                            ...e,
                            inCart: false,
                        }
                        : e
                })
            }
        case DELETE_FROM_LOCAL:
            return {
                ...state
            }
        default:
            return state
    }
}