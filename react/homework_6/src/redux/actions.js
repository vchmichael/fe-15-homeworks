//TYPES
export const OPEN_MODAL = 'OPEN_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'
export const ADD_FAVORITES = 'ADD_FAVORITES'
export const ADD_TO_CART = 'ADD_TO_CART '
export const GET_PRODUCTS = 'GET_PRODUCTS '
export const REMOVE_FROM_FAVORITES = 'REMOVE_FROM_FAVORITES'
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'
export const DELETE_FROM_LOCAL = 'DELETE_FROM_LOCAL'

//ACTIONS

export const openModal = () => (dispatch) => {
    dispatch({type: OPEN_MODAL});
}

export const closeModal = () => (dispatch) => {
    dispatch({type: CLOSE_MODAL});
}

export const getData = () => {
    return (dispatch) => {
        fetch('./goods.json')
            .then(res => res.json())
            .then(data => {
                const fullData = data.map(e => {
                    return localStorage.getItem(`favorites${e.article}`)
                        ? {
                            ...e,
                            inFavorites: true
                        }
                        : localStorage.getItem(`cart${e.article}`)
                            ? {
                                ...e,
                                inCart: true
                            }
                            : e
                })
                dispatch({type: GET_PRODUCTS, payload: fullData})
            })
    };
}


export const addToFavorites = (id) => (dispatch) => {
    dispatch({type: ADD_FAVORITES, payload: id});
    localStorage.setItem(`favorites${id}`, id)
}

export const addToCart = (id) => (dispatch) => {
    localStorage.setItem(`cart${id}`, id)
    dispatch({type: ADD_TO_CART, payload: id});
}

export const removeFromFavorites = (id) => (dispatch) => {
    dispatch({type: REMOVE_FROM_FAVORITES, payload: id});
    localStorage.removeItem(`favorites${id}`)
}

export const removeFromCart = (id) => (dispatch) => {
    localStorage.removeItem(`cart${id}`)
    dispatch({type: REMOVE_FROM_CART, payload: id});
}

export const removeFromStorage = (ids) => (dispatch) => {
    ids.forEach( e => localStorage.removeItem(`cart${e}`))
    dispatch(getData())
}
