const initState = {
    type: 'modal-success',
    isActive: false
}


export function modalReducer(state = initState, action) {
    switch (action.type) {
        case "OPEN_MODAL":
            return {
                ...state,
                isActive: true
            }
        case "CLOSE_MODAL":
            return {
                ...state,
                isActive: false
            }
        default:
            return state
    }
}