import React from 'react';
import './Loader.scss'
function Loader(props) {
    return (
        <div className="wrap">
            <div className="loading">
                <div className="bounceball"></div>
                <div className="text">LOADING</div>
            </div>
        </div>
    );
}

export default Loader;