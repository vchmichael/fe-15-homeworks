import React from 'react';
import {Form, withFormik} from 'formik';
import * as yup from 'yup';
import './OrderForm.scss'
import OrderFormInput from "../OrderFormInput/OrderFormInput";
import {connect} from "react-redux";
import {openModal, removeFromStorage} from "../../redux/actions";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";


const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
const nameRegExp = /^[a-z ,.'-]+$/i


const formSchema = yup.object().shape({
    firstname: yup
        .string()
        .required("This field is required")
        .matches(nameRegExp, 'First name is invalid')
        .max(20, 'First name must be shorter then 20 symbols'),
    lastname: yup
        .string()
        .required("This field is required")
        .matches(nameRegExp, 'Last name is invalid')
        .max(20, 'Last name must be shorter then 20 symbols'),
    age: yup
        .number()
        .min(18, 'You must be over 18 years old')
        .required("This field is required"),
    address: yup
        .string()
        .required("This field is required"),
    phone: yup
        .string()
        .matches(phoneRegExp, 'Phone number is not valid')
        .required("This field is required"),
})

const handleSubmit = (values, {props, setSubmitting}) => {
    console.log(values, props.products);
    let ids = props.products.map(e => {
        return e.article
    })
    props.dispatch(removeFromStorage(ids))
    setSubmitting(false)
    props.dispatch(openModal())
}

const OrderForm = (props) => {

    const {products, handleSubmit, modal} = props;
    let totalPrice = 0;
    products.forEach(e => totalPrice += +e.price)
    return (
        <>
            <Form className='order-form' onSubmit={handleSubmit}>
                <OrderFormInput type='text' name='firstname' placeholder='Your first name'/>
                <OrderFormInput type='text' name='lastname' placeholder='Your last name'/>
                <OrderFormInput type='text' name='age' placeholder='Your age'/>
                <OrderFormInput type='text' name='address' placeholder='Delivery address'/>
                <OrderFormInput type='number' name='phone' placeholder='Phone number'/>
                <span className='form-info'>Amount: {products.length}</span>
                <span className='form-info'>Total: {totalPrice} $</span>
                <button className='btns__btn checkout' type='submit'>Checkout</button>
            </Form>
        </>
    );
}

const mainForm = withFormik({
    mapPropsToValues: (props) => ({
        firstname: '',
        lastname: '',
        age: '',
        address: '',
        phone: '',
    }),
    validationSchema: formSchema,
    handleSubmit
})(OrderForm)

const FullOrderForm = connect()(mainForm)

export default FullOrderForm;