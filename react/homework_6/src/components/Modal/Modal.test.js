import React from 'react';
import { shallow } from 'enzyme';
import Modal from './Modal';




describe('Modal component', () => {
    it('Should have appropriate text', () => {
        const modal = shallow(
            <Modal
                title={'Some title'}
                body={'Some body here'}
            />)

        expect(modal.find('.modal-success__header__title').text()).toEqual('Some title');
        expect(modal.find('.modal-success__body').text()).toEqual('Some body here');
    })
    it('Should have close button', () => {
        const modal = shallow(
            <Modal
                closeButton={true}
            />)

        expect(modal.find('.btns__btn')).toHaveLength(1);
    })

})
