import React from 'react';
import './Header.scss'
import HeaderMenu from "../HeaderMenu/HeaderMenu";


const  Header =()=>  {

        return (
                <div className='header'>
                    <img className='header__logo' src='/logo.png' alt="Our logo"/>
                    <HeaderMenu />
                </div>
        );
}

export default Header;