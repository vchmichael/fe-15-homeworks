import React from 'react';
import GoodItem from '../GoodItem/GoodItem'
import './GoodsList.scss'


const GoodsList = (props) => {
    const {products} = props
    return (
        <>
            <div className="wrapper">
                <div className='our-products'>
                    {products && products.map(e => <GoodItem product={e} key={e.article} />)}
                </div>
            </div>
        </>
    );
}

// mapStateToProps
// const mapStoreToProps = (state) => ({
//     products: state.products.data
// })

export default GoodsList;