import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Button.scss'

class Button extends Component {
    render() {
        const {text, backgroundColor, fn, addCart} = this.props;
        const style = {
            backgroundColor: backgroundColor
        }
        return (

            <button
                onClick={() => {
                    fn();
                    if(addCart) {
                        addCart();
                    }
                }}
                className="btns__btn"
                style={style}>{text}
            </button>

        );
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    fn: PropTypes.func,
    text: PropTypes.string
};

export default Button;