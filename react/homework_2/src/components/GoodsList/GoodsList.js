import React, {Component} from 'react';
import GoodItem from '../GoodItem/GoodItem'
import './GoodsList.scss'

class GoodsList extends Component {
    render() {
        const {products, fn, addCart } = this.props
        return (
            <div className='our-products'>
                {products.map(e => <GoodItem product={e} fn={fn} key={e.article} addCart={addCart}/>)}
            </div>
        );
    }
}

export default GoodsList;