import React, {Component} from 'react';
import Button from "../Button/Button";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { faStar as starIcon } from "@fortawesome/free-regular-svg-icons"
import './GoodItem.scss'

class GoodItem extends Component {
     state ={
        onFavorite:false
    }

    render() {
        const {product, fn, addCart} = this.props
        return (
                <div className="product-card">
                    <div className='product-header'>
                        <span className='article'>Article: {product.article}</span>
                        <a target="_blank" href="#"><img
                            src={product.imageUrl}/></a>
                        <h2>{product.name}</h2>
                        <span>Color: {product.color}</span>
                    </div>
                    <div className="product-bio">
                        <h4>{product.price} {product.currency}</h4>
                        <span onClick={this.addToFavorites.bind(this)}>
                            {this.state.onFavorite && <FontAwesomeIcon icon={faStar}  className='favorites' />}
                            {!this.state.onFavorite && <FontAwesomeIcon icon={starIcon}  className='favorites' />}
                        </span>
                        <Button
                            text={'Add to cart'}
                            backgroundColor={'#C70039'}
                            fn={fn}
                            addCart={() => addCart(product.article)}
                        />

                    </div>
                </div>
        );
    }
    addToFavorites() {
        this.setState((curState) => {
            return {
                onFavorite:!curState.onFavorite
            }})

         if(!localStorage.getItem(`favorites${this.props.product.article}`)) {
             localStorage.setItem(`favorites${this.props.product.article}`, true)
         } else {
             localStorage.removeItem(`favorites${this.props.product.article}`)
         }

    }

}

export default GoodItem;