import React, {Component, Fragment} from 'react';
import Modal from './components/Modal/Modal'
import './App.scss'
import Button from "./components/Button/Button";
import GoodsList from "./components/GoodsList/GoodsList";


class App extends Component {
    state = {
        successModal: {
            title: 'Success!',
            body: "This product has been added to cart !",
            closeButton: true,
            actions: [
                <Button
                    key={0}
                    text={'OK'}
                    backgroundColor={'#00000030'}
                    fn={() => this.closeModal()}
                />,
            ],
            type: 'modal-success',
        },
        deleteModal: {
            title: 'Do you want to delete this file?',
            body: `Once you delete this file, it won’t be possible to undo this action.
               Are you sure you want to delete it?`,
            closeButton: true,
            actions: [
                <Button
                    key={0}
                    text={'OK'}
                    backgroundColor={'#00000030'}
                />,
                <Button
                    key={1}
                    text={'Cancel'}
                    backgroundColor={'#00000030'}
                />
            ],
            type: 'modal-delete',
        },
        isActive: false,
        activeModal: null,
        products: null,
        cart: null
    }

    componentDidMount() {
        fetch('./goods.json')
            .then(res => res.json())
            .then(data =>
                this.setState({products: data})
            )
    }

    render() {
        const whichModal = this.state[this.state.activeModal]

        return (
            <Fragment>
                <div className="wrapper">
                    <h2 className='section-title'>Our products</h2>
                    {this.state.products &&
                    <GoodsList products={this.state.products} fn={this.openSuccessModal.bind(this)} addCart={this.addToCart.bind(this)}/>}
                </div>
                {this.state.isActive && <Modal
                    title={whichModal.title}
                    body={whichModal.body}
                    actions={whichModal.actions}
                    closeBtn={whichModal.closeButton}
                    closeFn={this.closeModal.bind(this)}
                    type={whichModal.type}
                />}
            </Fragment>
        );
    }

    openSuccessModal() {
        this.setState({
            isActive: true,
            activeModal: 'successModal'
        })

    }

    openDeleteModal() {
        this.setState({
            isActive: true,
            activeModal: 'deleteModal'
        })

    }

    closeModal() {
        this.setState((curState) => {
            return {
                ...curState,
                isActive: !curState.isActive
            }
        });

    }
    addToCart(id) {
        localStorage.setItem(`cart${id}`, true)

    }

}

export default App;
