const {src, dest, series, parallel, watch} = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const postcss = require('gulp-postcss');
const uncss = require('postcss-uncss');
const del = require('del');
const browserSync = require('browser-sync').create();
const minify = require('gulp-minify');
const concat = require('gulp-concat');
const autoprefixer = require('autoprefixer');
const imagemin = require('gulp-imagemin');



function copyHtml() {
  return src('./src/index.html')
    .pipe(dest('./dist'))
    .pipe(browserSync.reload({ stream: true })); 
}

function buildJs() {
  return src('./src/js/**.js')
    .pipe(concat('scripts.js'))
    .pipe(minify({ext:{
        min:'.min.js'
    },noSource: true},))
    .pipe(dest('./dist/js'))
    .pipe(browserSync.reload({ stream: true })); 
}

function buildScss() {
  return src('./src/scss/*.scss')
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(concat('styles.min.css'))
    .pipe(postcss([
      uncss({html: ['./dist/index.html']}), autoprefixer()]))
    .pipe(dest('./dist/css'))
    .pipe(browserSync.stream());

}

function copyAssets() {
  return src('./src/assets/**')
    .pipe(dest('./dist/assets'))
}

function emptyDist() {
  return del('./dist/**');
}

function imageMinimaze() {
    return src('src/assets/img/*')
    .pipe(imagemin())
    .pipe(dest('dist/assets/img'))
}

const build = series(
  emptyDist,
  parallel(
    series(copyHtml , buildScss),
    copyAssets,
    imageMinimaze,
    buildJs
  )
);


function serve() {
  browserSync.init({
    server: './dist'
  });

  watch(['./src/index.html'], copyHtml);
  watch(['./src/scss/*.scss'], buildScss);
  watch(['./src/js/*.js'], buildJs);
}

const dev = series(
    serve
);

exports.build = build;
exports.dev = dev;
exports.default = series(build, dev); 
