const btn = document.querySelector('#contact')

btn.addEventListener('click', () => {
    let date = new Date(Date.now() + 300000)
    date = date.toUTCString();
    document.cookie = "experiment=novalue; expires=" + date;
    if(getCookie("new-user")) {
        document.cookie = "new-user=false"
    } else {
        document.cookie = "new-user=true"
    }
    console.log(document.cookie)
})
function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));

    return matches ? decodeURIComponent(matches[1]) : undefined;
}
