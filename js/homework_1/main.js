function Hamburger(size, stuffing) {
    if(arguments.length <2) {
        throw new HamburgerException("Not all arguments are passed")
    } else if(arguments[0].type !=='size') {
        throw new HamburgerException("Invalid size" + size)
    }else if(arguments[1].type !=='stuffing') {
        throw new HamburgerException("Invalid stuffing" + stuffing)
    } else {
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
    }
}

Hamburger.SIZE_SMALL = {price:50,calories:20,type:"size"}
Hamburger.SIZE_LARGE = {price:100,calories:40,type:"size"}
Hamburger.STUFFING_CHEESE = {price:10,calories:20,type:"stuffing"}
Hamburger.STUFFING_SALAD = {price:20,calories:5,type:"stuffing"}
Hamburger.STUFFING_POTATO = {price:15,calories:10,type:"stuffing"}
Hamburger.TOPPING_MAYO = {price:20,calories:50}
Hamburger.TOPPING_SPICE = {price:15,calories:0}


Hamburger.prototype.addTopping = function (topping) {
    if (!this.toppings.includes(topping)) {
             return this.toppings.push(topping);
         } else {
        throw new HamburgerException("duplicate topping" + topping)
    }
    if(topping === undefined) {
        throw new HamburgerException("Topping didn't passed")
    }
}

Hamburger.prototype.removeTopping = function (topping) {
    return this.toppings = this.toppings.filter(a => a !== topping)
}

Hamburger.prototype.getToppings = function (toppings) {
    return this.toppings;
}

Hamburger.prototype.getSize = function () {
    return this.size;
}

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
}

Hamburger.prototype.calculatePrice = function () {
    return this.size.price + this.stuffing.price + this.toppings.reduce((acc, val) => {
       return  acc + val.price
    }, 0)
}

Hamburger.prototype.calculateCalories = function () {
    return this.size.calories + this.stuffing.calories + this.toppings.reduce((acc, val) => {
       return  acc + val.calories
    }, 0)
}



function HamburgerException (message) {
    this.message = message;
    this.name = 'Hamburger Exсeption'
}

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1


// ERRORS

// // не передали обязательные параметры
// var h2 = new Hamburger(); // => HamburgerException: no size given
//
// // передаем некорректные значения, добавку вместо размера
// var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// // => HamburgerException: invalid size 'TOPPING_SAUCE'
//
// // добавляем много добавок
// var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // HamburgerException: duplicate topping 'TOPPING_MAYO'
