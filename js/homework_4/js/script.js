class Column {
    constructor(name) {
        this.name = name
        this.btnAdd = document.createElement('button');
        this.btnSort = document.createElement('button')
        this.cards = [];
    }
    create() {
        this.createColumn()
        this.addCard()
        this.sortCard()
    }
    createColumn() {
        let card = document.createElement('div');
        let title = document.createElement('input')
        title.setAttribute('value', this.name)
        card.classList.add('card');
        this.btnAdd.innerText = 'Add a card'
        this.btnAdd.classList.add('btn-add')
        this.btnSort.innerText = 'Sort'
        this.btnSort.classList.add('btn-sort')
        card.append(title,this.btnSort, this.btnAdd)
        document.querySelector('.container').append(card)
    }
    showContent() {
        this.cards.forEach(e => {
            this.btnAdd.before(e)
        })
    }
    removeContent() {
        this.cards.forEach(e => {
            e.remove()
        })
    }

    addCard(qualifiedName, value) {
        this.btnAdd.addEventListener('click', () => {
            let card = document.createElement('div');
            card.classList.add('card-text')
            let task = document.createElement('textarea')
            card.append(task)
            this.cards.push(card)
            this.showContent()
        })
    }
    sortCard() {
        this.btnSort.addEventListener('click', () => {
            this.cards.sort((a, b) => {
                if (a.children[0].value > b.children[0].value) {
                    return 1;
                }
                if (a.children[0].value < b.children[0].value) {
                    return -1;
                }
                return 0;
            })
            this.removeContent()
            this.showContent()
        })
    }
}

let newCol = new Column('New Column')
newCol.create()

//ADD COLUMN
let btnAddColumn = document.createElement('button')
btnAddColumn.innerText = "Add column";
btnAddColumn.classList.add('btn-add-column')
document.querySelector('body').append(btnAddColumn)

btnAddColumn.addEventListener('click', () => {
    let newCol = new Column('New Column')
    newCol.create()
})


//DRAG
const tasksListElement = document.querySelector(".card");
const taskElements = tasksListElement.querySelectorAll(".card-text");

for (const task of taskElements) task.draggable = true;

tasksListElement.addEventListener("dragstart", (evt) => {
    evt.target.classList.add("selected");
});

tasksListElement.addEventListener("dragend", (evt) => {
    evt.target.classList.remove("selected");
});

tasksListElement.addEventListener("dragover", (evt) => {
    // Разрешаем сбрасывать элементы в эту область
    evt.preventDefault();

    // Находим перемещаемый элемент
    const activeElement = tasksListElement.querySelector(".selected");
    // Находим элемент, над которым в данный момент находится курсор
    const currentElement = evt.target;
    // Проверяем, что событие сработало:
    // 1. не на том элементе, который мы перемещаем,
    // 2. именно на элементе списка
    const isMoveable =
        activeElement !== currentElement &&
        currentElement.classList.contains("card-text");

    // Если нет, прерываем выполнение функции
    if (!isMoveable) return;

    // evt.clientY - вертикальная координата курсора в момент, когда сработало событие
    const nextElement = getNextElement(evt.clientY, currentElement);

    // Проверяем, нужно ли менять элементы местами
    if (
        (nextElement && activeElement === nextElement.previousElementSibling) ||
        activeElement === nextElement
    ) {
        // Если нет, выходим из функции, чтобы избежать лишних изменений в DOM
        return;
    }

    // Вставляем activeElement перед nextElement
    tasksListElement.insertBefore(activeElement, nextElement);
});

const getNextElement = (cursorPosition, currentElement) => {
    // Получаем объект с размерами и координатами
    const currentElementCoord = currentElement.getBoundingClientRect();
    // Находим вертикальную координату центра текущего элемента
    const currentElementCenter =
        currentElementCoord.y + currentElementCoord.height / 2;

    // Если курсор выше центра элемента, возвращаем текущий элемент
    // В ином случае - следующий DOM-элемент
    const nextElement =
        cursorPosition < currentElementCenter
            ? currentElement
            : currentElement.nextElementSibling;

    return nextElement;
};








