class Hamburger {
    constructor(size, stuffing) {
        if (arguments.length < 2) {
            throw new HamburgerException("Not all arguments are passed")
        } else if (arguments[0].type !== 'size') {
            throw new HamburgerException("Invalid size " + size.name)
        } else if (arguments[1].type !== 'stuffing') {
            throw new HamburgerException("Invalid stuffing " + stuffing.name)
        } else {
            this._size = size;
            this._stuffing = stuffing;
            this._toppings = [];
        }
    }
    addTopping(topping) {
        if (!this._toppings.includes(topping)) {
            return this._toppings.push(topping);
        } else {
            throw new HamburgerException("duplicate topping " + topping.name)
        }
        if(topping === undefined) {
            throw new HamburgerException("Topping didn't passed")
        }
    }
    removeTopping(topping) {
        return this._toppings = this._toppings.filter(a => a !== topping)
    }
     getToppings() {
        return this._toppings;
    }
     getSize() {
        return this._size
    }
     getStuffing() {
        return this._stuffing
    }
     calculatePrice() {
        return this._size.price + this._stuffing.price + this._toppings.reduce((acc, val) => {
            return  acc + val.price
        }, 0)
    }
     calculateCalories() {
        return this._size.calories + this._stuffing.calories + this._toppings.reduce((acc, val) => {
            return  acc + val.calories
        }, 0)
    }

}

class HamburgerException extends Error {
    constructor(message) {
        super();
        this.message = message;
        this.name = 'Hamburger Exсeption'
    }
}


Hamburger.SIZE_SMALL = {price:50,calories:20,type:"size", name:'SIZE_SMALL'}
Hamburger.SIZE_LARGE = {price:100,calories:40,type:"size", name:'SIZE_LARGE'}
Hamburger.STUFFING_CHEESE = {price:10,calories:20,type:"stuffing", name:'STUFFING_CHEESE'}
Hamburger.STUFFING_SALAD = {price:20,calories:5,type:"stuffing", name:'STUFFING_SALAD'}
Hamburger.STUFFING_POTATO = {price:15,calories:10,type:"stuffing", name:'STUFFING_POTATO'}
Hamburger.TOPPING_MAYO = {price:20,calories:50, name:'TOPPING_MAYO '}
Hamburger.TOPPING_SPICE = {price:15,calories:0, name:'TOPPING_SPICE'}

//маленький гамбургер с начинкой из сыра
// var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// // добавка из майонеза
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // спросим сколько там калорий
// console.log("Calories: %f", hamburger.calculateCalories());
// // сколько стоит
// console.log("Price: %f", hamburger.calculatePrice());
// // я тут передумал и решил добавить еще приправу
// hamburger.addTopping(Hamburger.TOPPING_SPICE);
// // А сколько теперь стоит?
// console.log("Price with sauce: %f", hamburger.calculatePrice());
// // Проверить, большой ли гамбургер?
// console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// // Убрать добавку
// hamburger.removeTopping(Hamburger.TOPPING_SPICE);
// console.log("Have %d toppings", hamburger.getToppings().length); // 1

// ERRORS

// не передали обязательные параметры
// var h2 = new Hamburger(); // => HamburgerException: no size given
//
// // передаем некорректные значения, добавку вместо размера
// var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// // => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
// var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);