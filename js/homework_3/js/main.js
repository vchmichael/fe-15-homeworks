class Game {
    static EASY = {name: "easy", time: 1500};
    static MIDDLE = {name: "middle", time: 1000};
    static HARD = {name: "hard", time: 500};
    constructor(difficulty) {
        this.difficulty = difficulty;
        this.userScore = 0;
        this.computerScore = 0;
        this.intervalId = 0;
    }
    startGame() {
        this.removeBtns()
        this.createBoard()
        this.gameControl()
        if(this.intervalId === 0) {
            this.intervalId = setInterval(this.gameProcess.bind(this), this.difficulty.time)
        }
    }
    createBoard () {
        let table = document.createElement('table'), tr, td, row, cell;
        for (row = 0; row < 10; row++) {
            tr = document.createElement('tr');
            for (cell = 0; cell < 10; cell++) {
                td = document.createElement('td');
                tr.appendChild(td);
            }
            table.appendChild(tr);
        }
        document.querySelector('.game').appendChild(table);
    }
    removeBtns() {
        easyBtn.style.display = 'none'
        middleBtn.style.display = 'none'
        hardBtn.style.display = 'none'
    }
    gameProcess() {
        let row = Math.floor(Math.random() * (10 - 1 + 1) + 1)
        let column = Math.floor(Math.random() * (10 - 1 + 1) + 1)
        let cell = document.querySelector('tr:nth-child('+ row + ') td:nth-child(' + column + ')');
        if(!(cell.classList.contains('userpoint') || cell.classList.contains('computerpoint'))){
            cell.classList.add('wait')
        }
            cell.addEventListener('click', () => {
                if(cell.classList.contains('wait')) {
                cell.classList.remove('wait')
                cell.classList.add('userpoint')
                this.userScore++;
                this.gameControlRemove()
                this.gameControl()
            }
        })
        setTimeout(() =>{
            if(cell.classList.contains('wait')) {
                cell.classList.remove('wait')
                cell.classList.add('computerpoint')
                this.computerScore++;
                this.gameControlRemove()
                this.gameControl()
            }
        }, this.difficulty.time)
        if(this.userScore > 50 || this.computerScore > 50) {
            this.endOfTheGame()
        }
    }
    gameControl() {
        let uScore = document.createElement('span');
        let cScore = document.createElement('span');
        uScore.innerText = "User: " + `${this.userScore}`;
        cScore.innerText = "Computer: " + `${this.computerScore}`;
        document.querySelector('body').append(uScore,cScore)
    }
    gameControlRemove() {
        document.querySelectorAll('span').forEach(e => e.remove())
    }
    endOfTheGame() {
        clearInterval(this.intervalId)
        this.intervalId = 0;
        let winnerText = document.createElement('h3')
        if(this.userScore > 50) {
            winnerText.innerText = 'YOU WIN! Reload page for new game'
            document.querySelector('body').append(winnerText)
        } else {
            winnerText.innerText = 'YOU LOSE. Reload page for new game'
            document.querySelector('body').append(winnerText)
        }
    }

}

// CONTROLS
let easyBtn = document.createElement('button')
easyBtn.innerText = "EASY"
let middleBtn = document.createElement('button')
middleBtn.innerText = "MIDDLE"
let hardBtn = document.createElement('button')
hardBtn.innerText = "HARD"
document.querySelector('body').prepend(easyBtn, middleBtn, hardBtn)


//START GAME
easyBtn.addEventListener('click', () => {
    let newGame = new Game(Game.EASY)
    newGame.startGame()
})

middleBtn.addEventListener('click', () => {
    let newGame = new Game(Game.MIDDLE)
    newGame.startGame()
})

hardBtn.addEventListener('click', () => {
    let newGame = new Game(Game.HARD)
    newGame.startGame()
})

