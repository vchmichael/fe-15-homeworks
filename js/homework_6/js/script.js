class App {
    constructor() {
        this.app = document.querySelector('.app');
        this.feed = document.createElement('div');
        this.btnAdd = document.createElement('button');
    }

    init() {
        this.btnAdd.classList.add('add-btn');
        this.btnAdd.innerText = 'Add card';
        this.feed.classList.add('feed');
        this.app.append(this.feed);
        this.feed.before(this.btnAdd);
        this.getCards()
        this.addCard()
    }

    getCards() {
        let cards = fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())

        let users = fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())

        Promise.all([cards, users])
            .then(
                values => {
                    const posts = values[0].map(post => {
                        const users = values[1].find(user => {
                            return user.id === post.userId
                        })
                        const firstName = users.name.split(' ')[0];
                        const lastName = users.name.split(' ')[1];
                        const email = users.email
                        const userName = users.userName
                        return {...post, firstName, lastName, email, userName}
                    })
                    this.sortCards(posts).forEach(e => new Card(e).initCards())
                },
                reason => {
                    console.log(reason);
                });
    }

    sortCards(posts) {
        return posts.sort((a, b) => a.id > b.id ? -1 : 1)
    }

    addCard() {
        let postTitle = document.createElement('input')
        let postBody = document.createElement('textarea')
        let closeBtn = document.createElement('button')
        let addPostBtn = document.createElement('button')
        let modal = document.createElement('div')
        postTitle.setAttribute('placeholder', 'Post title')
        postBody.setAttribute('placeholder', 'Message')
        modal.classList.add('modal');
        addPostBtn.innerText = "Add post";
        addPostBtn.classList.add('add-btn')
        closeBtn.classList.add('close-btn')
        closeBtn.innerText = 'X';
        modal.append(postTitle, postBody, closeBtn, addPostBtn)
        this.btnAdd.addEventListener('click', () => this.app.append(modal))
        closeBtn.addEventListener('click', () => modal.remove())
        addPostBtn.addEventListener('click', () => {
            fetch('https://jsonplaceholder.typicode.com/posts', {
                method: 'POST',
                body: JSON.stringify({
                    title: postTitle.value,
                    body: postBody.value,
                    userId: 1
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
                .then(response => response.json())
                .then(json => {
                    let post = {
                        title: json.title,
                        body: json.body,
                        userName: 'Bert',
                        firstName: 'Leanne',
                        lastName: 'Graham',
                        email: 'Sincere@april.biz',
                        id: json.id,
                        userId: 1
                    }
                    console.log(post)
                    new Card(post).initCards()
                    modal.remove()
                })
        })
    }

}

class Card extends App {
    constructor({title, body, userName, firstName, lastName, email, id, userId}) {
        super()
        this.title = title;
        this.description = body;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userEmail = email;
        this.userId = userId;
        this.cardId = id;
        this.btnEdit = document.createElement('button');
        this.btnDelete = document.createElement('button');
        this.btnSave = document.createElement('button');
        this.cardTitle = document.createElement('h2');
        this.cardDescription = document.createElement('p');
    }

    initCards() {
        this.createCard()
        this.deleteCard()
        this.editCard()
    }

    createCard() {
        this.cardTitle.innerText = this.title;
        this.cardTitle.classList.add('cardTitle')
        this.cardDescription.classList.add('description')
        this.cardDescription.innerText = this.description;
        let fullName = document.createElement('span');
        fullName.classList.add('fullname')
        fullName.innerText = this.firstName + " " + this.lastName;
        let userEmail = document.createElement('span');
        userEmail.classList.add('userEmail')
        userEmail.innerText = this.userEmail;
        this.btnEdit.classList.add('edit-btn')
        this.btnEdit.innerText = 'Edit'
        this.btnDelete.classList.add('delete-btn')
        this.btnDelete.innerText = "Delete"
        this.renderCard(this.cardTitle, this.cardDescription, name, fullName, userEmail, this.btnEdit, this.btnDelete)
    }

    renderCard(cardTitle, cardDescription, name, fullName, userEmail, btnEdit, btnDelete) {
        const cardWrapper = document.createElement('div');
        cardWrapper.classList.add('card')
        cardWrapper.append(cardTitle, cardDescription, name, fullName, userEmail, btnEdit, btnDelete)
        //this.feed - doesn't work, why?
        document.querySelector('.feed').prepend(cardWrapper)
    }

    editCard() {
        this.btnEdit.addEventListener('click', onEdit.bind(this))

        function onEdit() {
            this.cardTitle.setAttribute('contenteditable', true)
            this.cardDescription.setAttribute('contenteditable', true)
            this.cardTitle.focus()
            this.cardTitle.classList.add('editable')
            this.cardDescription.classList.add('editableds')
            this.btnSave.classList.add('save-btn')
            this.btnSave.innerText = 'Save'
            this.btnEdit.parentElement.append(this.btnSave)
            this.btnEdit.remove()
        }

        this.btnSave.addEventListener('click', onSave.bind(this))

        function onSave() {
            fetch(`https://jsonplaceholder.typicode.com/posts/${this.cardId}`, {
                method: 'PUT',
                body: JSON.stringify({
                    id: this.cardId,
                    title: this.cardTitle.innerText,
                    body: this.cardDescription.innerText,
                    userId: this.userId
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
                .then(response => response.json())
                .then(json => {
                    this.cardTitle.innerText = json.title
                    this.cardDescription.innerText = json.body
                })
            this.cardTitle.classList.remove('editable')
            this.cardDescription.classList.remove('editableds')
            this.btnSave.remove()
            this.btnDelete.parentElement.append(this.btnEdit);
            this.btnSave.removeEventListener('click', onSave.bind(this))

        }
    }

    deleteCard() {
        this.btnDelete.addEventListener('click', onRemove.bind(this))

        function onRemove() {
            fetch(`https://jsonplaceholder.typicode.com/posts/${this.cardId}`, {
                method: 'DELETE',
            })
            this.btnDelete.parentElement.remove()
            this.btnDelete.removeEventListener('click', onRemove.bind(this))
        }
    }
}

let app = new App()
app.init()










