const checkBtn = document.createElement('button');
const container = document.querySelector('.container');
let info = document.createElement('span')
checkBtn.innerText = "Check IP";
container.append(checkBtn);

checkBtn.addEventListener('click', checkIp)

async function checkIp() {
    let response = await fetch('https://api.ipify.org/?format=json')
    let data = await response.json();
    let {ip} = data;

    await getIpDetails()

    async function getIpDetails() {
        let endPoint = `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district&lang=ru`
        let response = await fetch(endPoint);
        let data = await response.json()
        console.log(data)
        let {city, continent, country, district, regionName} = data;
        info.innerText = `Континент: ${continent}, Страна: ${country}, Город: ${city}, Регион: ${regionName}, Район:${district}`
        checkBtn.after(info)
    }
}