let mainContainer = document.querySelector('.container');
let filmList = document.createElement('ul');

mainContainer.append(filmList);



let req = new XMLHttpRequest();

req.open("GET","https://swapi.dev/api/films/");
req.responseType = 'json';
req.send();

req.onload = function() {
    if (req.status != 200) {
        let errorTitle = document.createElement('h2')
        errorTitle.innerText = `Ошибка ${req.status}: ${req.statusText}`
        mainContainer.append(errorTitle)
    } else {
        req.response.results.forEach(el => {
            let filmItem = document.createElement('li');
            let filmBlock = document.createElement('div')

            filmList.append(filmItem);
            filmItem.append(filmBlock);

            let filmTitle = document.createElement('h2')
            let episodeID = document.createElement('p')
            let openingCrawl = document.createElement('p')
            filmTitle.innerText = `${el.title}`
            episodeID.innerText = `Episode ${el.episode_id}`
            openingCrawl.innerText = `${el.opening_crawl}`

            filmList.append(filmItem)
            filmBlock.append(filmTitle, episodeID, openingCrawl)

                el.characters.forEach( (links) => {let names = [];
                let req = new XMLHttpRequest();
                req.open("GET", links);
                req.responseType = 'json';
                req.send();
                req.onload = function() {
                    if (req.status != 200) {
                        let errorTitle = document.createElement('h2')
                        errorTitle.innerText = `Ошибка ${req.status}: ${req.statusText}`
                        mainContainer.append(errorTitle)
                        } else {
                            names.push(req.response.name)
                        episodeID.before(names)
                        }
                    }
                    req.onprogress = function () {
                        let loader = document.createElement('div');
                        loader.classList.add('loader');
                        episodeID.before(loader)
                    }
            })


        })
    }

};


req.onerror = function() {
    let errorTitle = document.createElement('h2')
    errorTitle.innerText = 'Something went wrong.'
    mainContainer.append(errorTitle)
};

