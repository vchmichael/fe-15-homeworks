let mainContainer = document.querySelector('.container');
let filmList = document.createElement('ul');

mainContainer.append(filmList);


fetch('https://swapi.dev/api/films/')
    .then(response => response.json())
    .then(json => {
        console.log(json)
        json.results.forEach(el => {
            let filmItem = document.createElement('li');
            let filmBlock = document.createElement('div')

            filmList.append(filmItem);
            filmItem.append(filmBlock);

            let filmTitle = document.createElement('h2')
            let episodeID = document.createElement('p')
            let openingCrawl = document.createElement('p')
            filmTitle.innerText = `${el.title}`
            episodeID.innerText = `Episode ${el.episode_id}`
            openingCrawl.innerText = `${el.opening_crawl}`

            filmList.append(filmItem)
            filmBlock.append(filmTitle, episodeID, openingCrawl)
            let loader = document.createElement('div');
            loader.classList.add('loader');
            episodeID.before(loader)
            let users = el.characters.map(link => {
                return fetch(link).then(response => response.json())
            })
            Promise.all(users)
                .then(values => {
                    values.forEach(e => episodeID.before(e.name + ', '))
                    loader.remove()
                })

        })

    })